// ==== GLOBAL : Generic functions
function unlockAnimationByPagePosition(section){
	return document.body.scrollTop>section.offsetTop-200 || document.documentElement.scrollTop>section.offsetTop-200
}

// ==== GLOBAL : The minimum height of a section equals to the height of the window, without #section
Array.from(document.getElementsByTagName('section')).forEach(function(section) {
	if(window.innerWidth>550 && section.id!='contact'){section.style.minHeight=window.innerHeight+'px';}
});

// ==== WELCOME : Appear effect
document.getElementById('welcome').style.transform='skew(7deg,7deg)';
function welcomeAppear() {
	document.getElementById('welcome').style.transform='skew(0)';
}

// ==== MYSELF : Appear effect
var myself=document.getElementById('myself');
Array.from(myself.getElementsByTagName('li')).forEach(function(li){
	li.style.opacity=0;
});
function myselfAppear() {
	if(unlockAnimationByPagePosition(myself)) {
		let lis=myself.getElementsByTagName('li');
		for(let i=0;i<lis.length;i++){
			setTimeout(function(){lis[i].style.opacity=1;},1000*i);
		}
	}
}

// ==== ASSET : Background display
var asset=document.getElementById('asset');
function assetBackground() {
	if(unlockAnimationByPagePosition(asset) && window.innerWidth>550) {
		asset.style.animation='assetBackground 0.5s linear forwards';
	}
}

// ==== EXPERIENCE : Appear effect
var experience=document.getElementById('experience');
Array.from(experience.getElementsByTagName('aside')).forEach(function(aside){
	aside.style.marginLeft='120%';
});
function experienceAppear() {
	if(unlockAnimationByPagePosition(experience)) {
		let asides=experience.getElementsByTagName('aside');		
		for(let i=0;i<asides.length;i++){
			setTimeout(function(){asides[i].style.marginLeft=0;},200*i);
		}
	}
}

// ==== HOBBIE : Appear effect
var hobbie=document.getElementById('hobbie');
Array.from(hobbie.getElementsByTagName('aside')).forEach(function(aside){
	aside.style.transform='rotateY(90deg)';
});
function hobbieAppear() {
	if(unlockAnimationByPagePosition(hobbie)) {
		let asides=hobbie.getElementsByTagName('aside');
		for(let i=0;i<asides.length;i++){
			setTimeout(function(){asides[i].style.transform='rotateY(0)';},300*i);
		}
	}
}

// ==== PARTNER : Appear effect
var partner=document.getElementById('partner');
Array.from(partner.getElementsByTagName('li')).forEach(function(li){
	li.style.marginLeft='120%';
	li.getElementsByTagName('img')[0].style.transform='rotate(1440deg)';
});
function partnerAppear() {
	if(unlockAnimationByPagePosition(partner)) {
		let lis=partner.getElementsByTagName('li');
		for(let i=0;i<lis.length;i++){
			setTimeout(function(){
				lis[i].style.marginLeft=0;
				lis[i].getElementsByTagName('img')[0].style.transform='rotate(0deg)';
			},300*i);
		}
	}
}

// ==== Execution of fonctions
setTimeout(welcomeAppear,1000);
for(let f of [myselfAppear,assetBackground,experienceAppear,hobbieAppear,partnerAppear]) {
	for(let e of ['scroll','resize']) {
		window.addEventListener(e,f);
	}
}
